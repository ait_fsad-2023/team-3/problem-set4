# Lab 4: Content Management - Demonstration

*Note: This is a demonstration report for content management. Content management has not been integrated into our project yet.*

## Goal:

The objective of Lab 4 is to design and implement a content management system for our application.

## Instructions:

The instructions for this lab involve designing and implementing a skeletal content management system. The tasks include classifying content types, designing a content data model, specifying workflows, designing a versioning system, writing UATs, and providing a skeletal implementation.

## Step up 

### Modify the Article Model

```ruby
# app/models/article.rb
class Article < ApplicationRecord
  validates :title, :content, presence: true
end
```
We added validations to ensure that both title and content are present when creating or updating an article.
###  Add Status Enum to Article Model
```# app/models/article.rb
class Article < ApplicationRecord
  enum status: { draft: 0, published: 1, archived: 2 }

  validates :title, :content, presence: true
end
```
We introduced an enum for the status of an article, allowing us to categorize articles as draft, published, or archived.
###  Add Comment Model
```
rails generate scaffold Comment commenter:string body:text article:references
rails db:migrate
```
```# app/models/article.rb
class Article < ApplicationRecord
  has_many :comments, dependent: :destroy

  enum status: { draft: 0, published: 1, archived: 2 }

  validates :title, :content, presence: true
end
```
We generated a scaffold for comments, creating a model with attributes and a reference to the article. The relationships between articles and comments were defined.
###  Add Routes for Comments
```ruby
# config/routes.rb
Rails.application.routes.draw do
  resources :articles do
    resources :comments
  end

  root 'articles#index'
end
```
We added routes for comments nested under articles, enabling RESTful actions for managing comments in the context of an article.
```html
###  Update Article Show View
<!-- app/views/articles/show.html.erb -->
<p id="notice"><%= notice %></p>

<p>
  <strong>Title:</strong>
  <%= @article.title %>
</p>

<p>
  <strong>Content:</strong>
  <%= @article.content %>
</p>

<p>
  <strong>Status:</strong>
  <%= @article.status %>
</p>

<h2>Comments</h2>
<% @article.comments.each do |comment| %>
  <p>
    <strong>Commenter:</strong>
    <%= comment.commenter %>
  </p>
  <p>
    <strong>Comment:</strong>
    <%= comment.body %>
  </p>
<% end %>

<h2>Add a comment:</h2>
<%= form_with(model: [ @article, @article.comments.build ], local: true) do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>

<%= link_to 'Edit', edit_article_path(@article) %> |
<%= link_to 'Back', articles_path %>
```
We updated the article show view to display comments, allowing users to view existing comments and add new ones.
###  Update Article Controller

```ruby #class ArticlesController < ApplicationController
  before_action :set_article, only: %i[ show edit update destroy ]

  # GET /articles or /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1 or /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles or /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to article_url(@article), notice: "Article was successfully created." }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1 or /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to article_url(@article), notice: "Article was successfully updated." }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1 or /articles/1.json
  def destroy
    @article.destroy!

    respond_to do |format|
      format.html { redirect_to articles_url, notice: "Article was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def article_params
      params.require(:article).permit(:title, :content, :status)
    end
end
def new
  @article = Article.find(params[:article_id])
  @comment = Comment.new
end
```
We modified the article controller to build a new comment instance when showing an article, facilitating the addition of comments.
# Testing:
We have conducted thorough testing of the content management workflows to ensure they meet the specified requirements. Test cases were created for each workflow stage.

Testing involved both unit tests and integration tests using RSpec and Capybara. We verified that validations, associations, and routes are functioning correctly.


![Sample Image](https://gitlab.com/ait_fsad-2023/team-3/problem-set4/-/raw/main/images/article_created.png)

![Sample Image](https://gitlab.com/ait_fsad-2023/team-3/problem-set4/-/raw/main/images/create_new_article.png)

![Sample Image](https://gitlab.com/ait_fsad-2023/team-3/problem-set4/-/raw/main/images/added_comment.png)


# Deployment:
Note: This section discusses considerations for future deployment as content management is not yet integrated.

We plan to deploy the content management system once fully integrated into our application. Considerations will include database migrations, version compatibility, and user training.

# Conclusion:
> In this demonstration report, we have outlined the design and implementation of a content management system for our application. While the system is not integrated, the groundwork has been laid for seamless future integration.