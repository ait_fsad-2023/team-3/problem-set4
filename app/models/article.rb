# app/models/article.rb
class Article < ApplicationRecord
    has_many :comments, dependent: :destroy
  
    enum status: { draft: 0, published: 1, archived: 2 }
  
    validates :title, :content, presence: true
  end
  